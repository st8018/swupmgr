/*
 * PlatformSoftwareUpgradeManager.h
 *
 */

#ifndef PLATFORMSOFTWAREUPGRADEMANAGER_H_
#define PLATFORMSOFTWAREUPGRADEMANAGER_H_

#include <stdlib.h>
#include <vector>

#include "SoftwareUpgradeManager.h"

/**
 * This class is responsible to interface with platform software upgrade services.
 *
 * Singleton.
 */
class PlatformSoftwareUpgradeManager:
    public SoftwareUpgradeManager
{

    friend SoftwareUpgradeManager& SoftwareUpgradeManager::instance ();

private:

    PlatformSoftwareUpgradeManager ();

public:

    virtual ~PlatformSoftwareUpgradeManager ();

    /**
     * Executes the command as a shell command.
     * @param command The command to be exectued.
     * @return Retruns the status of shell process.
     */
    static int execShellCommand (const std::string& command);

    /**
     * Executes the command as a subprocess and gets the output of the executed command.
     * @param command The command to be executed.
     * @param output Output of the command.
     * @return Return 0 if it is success, otherwise -1.
     */
    static int execShellCommandWithOutput (const std::string& command, std::vector<std::string>& output);

    static void* downloadSWImageThread(void *arg);
    static void* installSWImageThread(void *arg);

    /**
     * This method initializes the HAL implementation of
     * PlatformSoftwareManager.
     */
    virtual void initHAL ();

    /**
     * This method deinitializes the HAL implementation of
     * PlatformSoftwareManager.
     */
    virtual void deinitHAL ();

    /**
     * This method intimates the Stack that it may continue to install a SW Upgrade image that has been downloaded.
     * @return bool denotes the installation process status.
     */
    virtual InstallSWImageResult installSWImage (bool afterReboot);

    /**
     * This method is used to Downloads the SW Image from the Server.
     * @param url denotes complete url path to the SW Image.
     * @param targetFileName denotes file path to save SW Image.
     * @return int denotes the download response value.
     */
    virtual DownloadSWImageResult downloadSWImage (const std::string& url, const std::string& userName, const std::string& password, const uint32_t fileSize, const std::string& targetFileName);

    /**
     * This method cancels the current SWUpgrade process.
     * The SW Image should be reverted to the previous SW Image
     * after calling this method and the STB may reboot if necessary.
     */
    virtual void cancelSWUpgrade ();
};

#endif /* PLATFORMSOFTWAREUPGRADEMANAGER_H_ */
