#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>

#include "swupMgr.h"
#include "swupMgrInternal.h"

#include "SoftwareUpgradeManager.h"

static IARM_Result_t _GetDownloadState(void *arg);
static IARM_Result_t _InstallFirmwareUpdate(void *arg);
static IARM_Result_t _InvokeSWUpgradeService(void *arg);
static IARM_Result_t _GetDownloadPercentage(void* arg);

IARM_Result_t IARM_SWUPMgr_Start(int argc, char *argv[])
{
    IARM_Result_t ret = IARM_RESULT_SUCCESS;

    ret = IARM_Bus_Init(IARM_BUS_SWUPMGR_NAME);
    if (ret != IARM_RESULT_SUCCESS) {
        return ret;
    }

    ret = IARM_Bus_Connect();
    if (ret != IARM_RESULT_SUCCESS) {
        return ret;
    }

    ret = IARM_Bus_RegisterEvent(IARM_BUS_SWUPMGR_EVENT_MAX);
    if (ret != IARM_RESULT_SUCCESS) {
        return ret;
    }

    ret = IARM_Bus_RegisterCall(IARM_BUS_SWUPMGR_API_GetDownloadState, _GetDownloadState);
    if (ret != IARM_RESULT_SUCCESS) {
        return ret;
    }
    ret = IARM_Bus_RegisterCall(IARM_BUS_SWUPMGR_API_InstallFirmwareUpdate, _InstallFirmwareUpdate);
    if (ret != IARM_RESULT_SUCCESS) {
        return ret;
    }
    ret = IARM_Bus_RegisterCall(IARM_BUS_SWUPMGR_API_InvokeSWUpgradeService, _InvokeSWUpgradeService);
    if (ret != IARM_RESULT_SUCCESS) {
        return ret;
    }
    ret = IARM_Bus_RegisterCall(IARM_BUS_SWUPMGR_API_GetDownloadPercentage, _GetDownloadPercentage);
    if (ret != IARM_RESULT_SUCCESS) {
        return ret;
    }
    return ret;
}

IARM_Result_t IARM_SWUPMgr_Loop()
{
    time_t curr = 0;
    IARM_Result_t ret = IARM_RESULT_SUCCESS;
    usleep(10 * 1000 * 1000);
    while (1) {
        time(&curr);
        usleep(300 * 1000 * 1000);
        printf("I-ARM SWUP Mgr: HeartBeat at %s\n", ctime(&curr));
    }

    return ret;
}

IARM_Result_t IARM_SWUPMgr_Stop()
{
    IARM_Result_t ret = IARM_RESULT_SUCCESS;

    ret = IARM_Bus_Disconnect();
    if (ret != IARM_RESULT_SUCCESS) {
        return ret;
    }

    ret = IARM_Bus_Term();
    if (ret != IARM_RESULT_SUCCESS) {
        return ret;
    }

    return ret;
}

static IARM_Result_t _GetDownloadState(void *arg)
{
    IARM_Bus_SWUPMgr_GetDownloadState_Param_t *param = (IARM_Bus_SWUPMgr_GetDownloadState_Param_t *)arg;
    bool result = false;
    IARM_Bus_SWUPMgr_SWUpgrade_State_t state;

    result = SoftwareUpgradeManager::instance().getDownloadState(state);
    if (!result) {
        param->upgradeState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_FAILED;
        strcpy(param->errorMessage, "Failed to call getDownloadState");
        return IARM_RESULT_SUCCESS;
    }

    param->upgradeState = state;

    return IARM_RESULT_SUCCESS;
}

static IARM_Result_t _InstallFirmwareUpdate(void *arg)
{
    IARM_Bus_SWUPMgr_InstallFirmwareUpdate_Param_t *param = (IARM_Bus_SWUPMgr_InstallFirmwareUpdate_Param_t *)arg;
    bool result = false;

    result = SoftwareUpgradeManager::instance().installSWImage(param->afterReboot);

    if (!result) {
        param->result = 1;
    } else {
        param->result = 0;
        strcpy(param->errorMessage, "Failed to call installSWImage");
    }

    return IARM_RESULT_SUCCESS;
}

static IARM_Result_t _InvokeSWUpgradeService(void *arg)
{
    IARM_Bus_SWUPMgr_InvokeSWUpgradeService_Param_t *param = (IARM_Bus_SWUPMgr_InvokeSWUpgradeService_Param_t *)arg;
    SoftwareUpgradeManager::DownloadRequest downloadRequest;

    printf("_InvokeSWUpgradeService is started\n");
    printf("commandKey=%s\n", param->commandKey);
    printf("url=%s\n", param->url);
    printf("userName=%s\n", param->userName);
    printf("password=%s\n", param->password);
    printf("fileSize=%d\n", param->fileSize);
    printf("targetFileName=%s\n", param->targetFileName);
    printf("delaySeconds=%d\n", param->delaySeconds);
    printf("successURL=%s\n", param->successURL);
    printf("failureURL=%s\n", param->failureURL);

    std::string url(param->url);

    downloadRequest.commandKey = param->commandKey;
    downloadRequest.downloadType = SoftwareUpgradeManager::DOWNLOAD;
    downloadRequest.imageUrl = url;

    SoftwareUpgradeManager::instance().invokeSWUpgradeService(downloadRequest);

    return IARM_RESULT_SUCCESS;
}

static IARM_Result_t _GetDownloadPercentage(void* arg)
{
    //TODO
}

void broadcastSWUpgradeStateChange(IARM_Bus_SWUPMgr_SWUpgrade_State_t currentState, IARM_Bus_SWUPMgr_SWUpgrade_State_t previousState)
{
    IARM_Result_t ret = IARM_RESULT_SUCCESS;
    IARM_Bus_SWUPMgr_EventData_t eventData;
    eventData.data.state.prevUpgradeState = previousState;
    eventData.data.state.newUpgradeState = currentState;
    ret = IARM_Bus_BroadcastEvent(IARM_BUS_SWUPMGR_NAME,
            IARM_BUS_SWUPMGR_EVENT_SWUPGRADE_STATE_CHANGED,
            (void*)&eventData,
            sizeof(eventData));
    printf("broadcastSWUpgradeStateChange,ret=%d\n", ret);
}
