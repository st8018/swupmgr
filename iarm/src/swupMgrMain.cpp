#include <stdlib.h>

#include "swupMgr.h"
#include "swupMgrInternal.h"

int main(int argc, char *argv[])
{
/*
    if (rdk_logger_init(NULL) != RDK_SUCCESS) {
        printf("Failed to call rdk_logger_init() in swupMgr.main()\n");
        exit(EXIT_FAILURE);
    }
*/

    IARM_SWUPMgr_Start(argc, argv);
    IARM_SWUPMgr_Loop();
    IARM_SWUPMgr_Stop();

    return 0;
}
