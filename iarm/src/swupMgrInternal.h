#ifndef _IARM_BUS_SWUPMGR_INTERNAL_H
#define _IARM_BUS_SWUPMGR_INTERNAL_H

#include "libIARM.h"
#include "rdk_debug.h"
#define LOG_COMP_NAME "LOG.TPC.SWUPMGR"

#define FATAL_LOG(...)      \
    RDK_LOG(RDK_LOG_FATAL, LOG_COMP_NAME, __VA_ARGS__)
#define ERROR_LOG(...)      \
    RDK_LOG(RDK_LOG_ERROR, LOG_COMP_NAME, __VA_ARGS__)
#define WARN_LOG(...)       \
    RDK_LOG(RDK_LOG_WARN, LOG_COMP_NAME, __VA_ARGS__)
#define NOTICE_LOG(...)     \
    RDK_LOG(RDK_LOG_NOTICE, LOG_COMP_NAME, __VA_ARGS__)
#define INFO_LOG(...)       \
    RDK_LOG(RDK_LOG_INFO, LOG_COMP_NAME, __VA_ARGS__)
#define DEBUG_LOG(...)      \
    RDK_LOG(RDK_LOG_DEBUG, LOG_COMP_NAME, __VA_ARGS__)
#define TRACE1_LOG(...)     \
    RDK_LOG(RDK_LOG_TRACE1, LOG_COMP_NAME, __VA_ARGS__)
    
IARM_Result_t IARM_SWUPMgr_Start(int argc, char *argv[]);
IARM_Result_t IARM_SWUPMgr_Loop();
IARM_Result_t IARM_SWUPMgr_Stop(void);

#endif /* _IARM_BUS_SWUPMGR_INTERNAL_H */
