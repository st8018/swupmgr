#ifndef _IARM_BUS_SWUPMGR_H
#define _IARM_BUS_SWUPMGR_H

#include "libIARM.h"
#include "libIBus.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define IARM_BUS_SWUPMGR_NAME    "SWUPMgr"  /* TPC SWUP manager IARM bus name */

/* Declare Published Events */

typedef enum _SWUPMgr_EventId_t {
    IARM_BUS_SWUPMGR_EVENT_SWUPGRADE_STATE_CHANGED = 0, /* Event to notify software upgrade state change */
    IARM_BUS_SWUPMGR_EVENT_MAX,                             /* Max event id from this module */
} IARM_Bus_SWUPMgr_EventId_t;

/* Possible software upgrade states */
typedef enum _SWUPMgr_SWUpgrade_State_t {
	IARM_BUS_SWUPMGR_SWUPGRADE_STATE_IDLE,
	IARM_BUS_SWUPMGR_SWUPGRADE_STATE_CONFIGURING,
	IARM_BUS_SWUPMGR_SWUPGRADE_STATE_PREPARING,
	IARM_BUS_SWUPMGR_SWUPGRADE_STATE_DOWNLOADING,
	IARM_BUS_SWUPMGR_SWUPGRADE_STATE_DOWNLOADED,
	IARM_BUS_SWUPMGR_SWUPGRADE_STATE_WRITING,
	IARM_BUS_SWUPMGR_SWUPGRADE_STATE_COMPLETED,
	IARM_BUS_SWUPMGR_SWUPGRADE_STATE_REBOOTING,
	IARM_BUS_SWUPMGR_SWUPGRADE_STATE_FAILED,
} IARM_Bus_SWUPMgr_SWUpgrade_State_t;

/* Event data*/
typedef struct _SWUPMgr_EventData_t {
    union {
        struct _state {
                /* Declare Event Data structure */
                IARM_Bus_SWUPMgr_SWUpgrade_State_t prevUpgradeState;
                IARM_Bus_SWUPMgr_SWUpgrade_State_t newUpgradeState;
        } state;
    } data;
} IARM_Bus_SWUPMgr_EventData_t;


/*
 * Declare RPC API names and their arguments
 */

#define IARM_BUS_SWUPMGR_MAX_PARAM_LEN 256

#define IARM_BUS_SWUPMGR_API_GetDownloadState "GetDownloadState"

/* Parameter for GetDownloadState call */
typedef struct _IARM_Bus_SWUPMgr_GetDownloadState_Param_t {
        IARM_Bus_SWUPMgr_SWUpgrade_State_t upgradeState;
        /* If upgradeState is IARM_BUS_SWUPMGR_SWUPGRADE_STATE_FAILED,
         * errorMessage will include detailed error messages
         */
        char errorMessage[IARM_BUS_SWUPMGR_MAX_PARAM_LEN];
} IARM_Bus_SWUPMgr_GetDownloadState_Param_t;

#define IARM_BUS_SWUPMGR_API_InstallFirmwareUpdate "InstallFirmwareUpdate"

/* Parameter for InstallFirmwareUpdate call */
typedef struct _IARM_Bus_SWUPMgr_InstallFirmwareUpdate_Param_t {
        int result; /* ture if result is 1, false if result is 0 */
        bool afterReboot; /* ture after install firmwarupdate, try to reboot. */
        /* If result is false,
         * errorMessage will include detailed error messages
         */
        char errorMessage[IARM_BUS_SWUPMGR_MAX_PARAM_LEN];
} IARM_Bus_SWUPMgr_InstallFirmwareUpdate_Param_t;

#define IARM_BUS_SWUPMGR_API_InvokeSWUpgradeService "InvokeSWUpgradeService"

/* Parameter for InvokeSWUpgradeService call */
typedef struct _IARM_Bus_SWUPMgr_InvokeSWUpgradeService_Param_t {
        char commandKey[32];
        char url[256];
        char userName[256];
        char password[256];
        unsigned int fileSize;
        char targetFileName[256];
        unsigned int delaySeconds;
        char successURL[256];
        char failureURL[256];
} IARM_Bus_SWUPMgr_InvokeSWUpgradeService_Param_t;

#define IARM_BUS_SWUPMGR_API_GetDownloadPercentage "GetDownloadPercentage"

typedef struct _IARM_Bus_SWUPMgr_GetDownloadPercentage_Param_t {
    /* returns 0 to 100 if DOWNLOADING state
     * returns -1 if not DOWNLOADING state */
    int result; 
} IARM_Bus_SWUPMgr_GetDownloadPercentage_Param_t;

/* This is called from SoftwareUpgradeManager to broadcast SWUpgradeStateChange Event */
void broadcastSWUpgradeStateChange(IARM_Bus_SWUPMgr_SWUpgrade_State_t currentState, IARM_Bus_SWUPMgr_SWUpgrade_State_t previousState);

#ifdef __cplusplus
}
#endif

#endif /* _IARM_BUS_SWUPMGR_H */
