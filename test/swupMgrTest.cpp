#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "libIBus.h"
#include "swupMgr.h"

static void _dummyEventHandler(const char *owner, IARM_EventId_t eventId, void *data, size_t len);
static void testGetDownloadState(void);
static void testInstallFirmwareUpdate(void);
static void testInvokeSWUpgradeService(void);

int main(int argc, char *argv[])
{
    time_t curr = 0;
    /*
     * Register and Connect to the IARM Bus.
     *
     */
    IARM_Bus_Init("SWUPMgrTest");
    
    IARM_Bus_Connect();

    /*
     * After connection, swupMgrTest can communicate with entities on the IARM Bus.
     * 1. Register Handlers for SWUPMgr's published events.
     * 2. Invoke RPC calls to SWUPMgr's published methods.
     */
    IARM_Bus_RegisterEventHandler(IARM_BUS_SWUPMGR_NAME,
            IARM_BUS_SWUPMGR_EVENT_SWUPGRADE_STATE_CHANGED,
            _dummyEventHandler);

    usleep(10 * 1000 * 1000);

    while (1) {
        testGetDownloadState();
        //testInstallFirmwareUpdate();
        testInvokeSWUpgradeService();

        time(&curr);
        usleep(60 * 1000 * 1000);
        printf("SWUPMgr Test: HeartBeat at %s\n", ctime(&curr));
    }

    IARM_Bus_Disconnect();
    IARM_Bus_Term();
}

static void _dummyEventHandler(const char *owner, IARM_EventId_t eventId,
        void *data, size_t len)
{

    if (strcmp(owner, IARM_BUS_SWUPMGR_NAME) == 0) {
        /* Handle SWUPMgr's events here */
        IARM_Bus_SWUPMgr_EventData_t *eventData = (IARM_Bus_SWUPMgr_EventData_t *)data;

        switch(eventId) {
            case IARM_BUS_SWUPMGR_EVENT_SWUPGRADE_STATE_CHANGED:
                printf("_dummyEventHandler,eventData=%d,%d\n",
                        eventData->data.state.prevUpgradeState,
                        eventData->data.state.newUpgradeState);
                break;
            default:
                printf("_dummyEventHandler,eventId=%d\n", eventId);
                break;
        }
    }
    else if (/* compare owner compare other names */1) {
        /* Handle Another owner's events here */
    }
    else {
    }
}

static void testGetDownloadState(void)
{
    IARM_Bus_SWUPMgr_GetDownloadState_Param_t param;

    /* populate param here */
    IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
            IARM_BUS_SWUPMGR_API_GetDownloadState,
            (void *)&param,
            sizeof(param));
    printf("upgradeState:%d\n", param.upgradeState);
    if (param.upgradeState == IARM_BUS_SWUPMGR_SWUPGRADE_STATE_FAILED) {
        printf("Software upgrade is failed due to error=%s\n", param.errorMessage);
    }
}

static void testInstallFirmwareUpdate(void)
{
    IARM_Bus_SWUPMgr_InstallFirmwareUpdate_Param_t param;

    /* populate param here */
    IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
            IARM_BUS_SWUPMGR_API_InstallFirmwareUpdate,
            (void *)&param,
            sizeof(param));
    printf("result:%d\n", param.result);
    if (param.result == 0) {
        printf("Installation of a SW upgrade image is failed due to error=%s\n", param.errorMessage);
    }
}

static void testInvokeSWUpgradeService(void)
{
    IARM_Bus_SWUPMgr_InvokeSWUpgradeService_Param_t param;

    strcpy(param.commandKey, "testCommandKey");
    strcpy(param.url, "http://10.12.1.134/swimags/HUMAX.TCI-100.PCI.bin.2013-12-10-01");
    strcpy(param.userName, "twc-rdk");
    strcpy(param.password, "humax-entropic");
    param.fileSize = 80 * 1024 * 1024;
    strcpy(param.targetFileName, "HUMAX.TCI-100.PCI.bin");
    param.delaySeconds = 60;
    strcpy(param.successURL, "");
    strcpy(param.failureURL, "");

    printf("Calling IARM_BUS_SWUPMGR_API_InvokeSWUpgradeService\n");
    /* populate param here */
    IARM_Bus_Call(IARM_BUS_SWUPMGR_NAME,
            IARM_BUS_SWUPMGR_API_InvokeSWUpgradeService,
            (void *)&param,
            sizeof(param));
    printf("Returned from IARM_BUS_SWUPMGR_API_InvokeSWUpgradeService\n");
}
