/*
 * SWUpgradeStateChangeEvent.h
 *
 */

#ifndef _SWUPGRADE_STATE_CHANGE_EVENT_H_
#define _SWUPGRADE_STATE_CHANGE_EVENT_H_

#include <stdint.h>
#include <string>

/**
 * represents software upgrade state machine.
 */
enum StateMachine
{
    SWUPGRADE_STATE_IDLE = 0,
    SWUPGRADE_STATE_ACQUIRING_IMAGE = 1,
    SWUPGRADE_STATE_UNPACKING_IMAGE = 2,
    SWUPGRADE_STATE_LOADING_IMAGE = 3,
    SWUPGRADE_STATE_WAITING_TO_FLASH = 4,
    SWUPGRADE_STATE_FLASHING_IMAGE = 5,
    SWUPGRADE_STATE_WAITING_FOR_REBOOT = 6,
    SWUPGRADE_STATE_COMPLETED = 7,
    SWUPGRADE_STATE_FAILED = 8,
    SWUPGRADE_STATE_WAITING_FOR_DOWNLOAD = 9,
    SWUPGRADE_STATE_WAITING_FOR_COMPLETING = 10,
    SWUPGRADE_STATE_WAITING_REBOOT_EVENT = 11
};

class SWUpgradeStateChangeEvent
{
    friend class SoftwareUpgradeManager;

public:
    SWUpgradeStateChangeEvent ();
    virtual ~SWUpgradeStateChangeEvent ();

private:
    // software upgrade state machine
    StateMachine state;
    // faultCode
    uint32_t faultCode;
    // faultString
    std::string faultString;
};

#endif /* _SWUPGRADE_STATE_CHANGE_EVENT_H_ */
