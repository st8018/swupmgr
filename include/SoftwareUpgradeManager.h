/*
 * SoftwareUpgradeManager.h
 *
 */

#ifndef SOFTWAREUPGRADEMANAGER_H_
#define SOFTWAREUPGRADEMANAGER_H_

#include <map>
#include <queue>
#include <stdio.h>
#include <pthread.h>
#include <stdint.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#include <cstdio>
#include <cstring>
#include <stdexcept>

#include "swupMgr.h"
#include "SWUpgradeCommand.h"
#include "SWUpgradeStateChangeEvent.h"

const std::string commandKey = "PendingDownloads";
const std::string transferCompleteMessage = "transfercompleted";

typedef std::map <time_t, SWUpgradeCommand*> SWUpgradeCommandMap;
typedef std::map <std::string, std::string> BundleAttributeInfo;

/**
 * This class is responsible for software upgrade process from end to end.
 *
 * Singleton.
 */
class SoftwareUpgradeManager
{
public:
    virtual ~SoftwareUpgradeManager ();

    void init ();
    void deinit ();

    /**
     * represents type of download - Immediate/Schedule Download.
     */
    enum DownloadType
    {
        DOWNLOAD = 0,
        SCHEDULE_DOWNLOAD = 1
    };

    /**
     * represents Schedule download modes.
     */
    enum ScheduleDownloadMode
    {
        AT_ANY_TIME = 1,
        IMMEDIATELY = 2,
        WHEN_IDLE = 3,
        CONFIRMATION_NEEDED = 4
    };

    /**
     * represents the ACS Failure Error Codes.
     */
    enum FaultCode
    {
        FAULTCODE_NO_FAULT = 0,
        FAULTCODE_REQUEST_DENIED = 9001,
        FAULTCODE_INTERNAL_ERROR = 9002,
        FAULTCODE_FT_FAILURE = 9010,
        FAULTCODE_FT_SERVER_AUTHENTICATION_FAILURE = 9012,
        FAULTCODE_FT_FAILURE_UNABLE_TO_JOIN_MULTICAST_GROUP = 9014,
        FAULTCODE_FT_FAILURE_UNABLE_TO_CONTACT_FILE_SERVER = 9015,
        FAULTCODE_FT_FAILURE_UNABLE_TO_ACCESS_FILE = 9016,
        FAULTCODE_FT_FAILURE_UNABLE_TO_COMPLETE_DOWNLOAD = 9017,
        FAULTCODE_FT_FAILURE_FILE_CORRUPTED = 9018,
        FAULTCODE_FT_FILE_AUTHENTICATION_FAILURE = 9019,
        FAULTCODE_FT_FAILURE_UNABLE_TO_COMPLETE_DOWNLOAD_WITHIN_WINDOWS = 9020
    };

    typedef enum _DownloadSWImageResult
    {
        DOWNLOAD_SW_IMAGE_STARTED = 0,
        DOWNLOAD_SW_IMAGE_NOT_STARTED = 1
    } DownloadSWImageResult;

    typedef enum _InstallSWImageResult
    {
        INSTALL_SW_IMAGE_STARTED = 0,
        INSTALL_SW_IMAGE_NOT_STARTED = 1
    } InstallSWImageResult;

    typedef struct _SWUpgradeStateChangeInfo
    {
        // software upgrade state machine
        StateMachine state;
        // faultCode
        uint32_t faultCode;
        // faultString
        std::string faultString;
    } SWUpgradeStateChangeInfo;

    /**
     * represents the Download/Schedule Download Request Structure.
     */
    struct Download_Request
    {
        /** Download type. */
        uint32_t downloadType;
        /** Download Image URL. */
        std::string imageUrl;
        /** Download command. */
        std::string commandKey;

        // first - window
        /** represents download start time. */
        uint32_t acquireStartTime;
        /** represents download stop time. */
        uint32_t acquireStopTime;

        // second - window
        /** Download start time. */
        uint32_t applyStartTime;
        /** Download stop time. */
        uint32_t applyStopTime;
        /** applying mode. */
        uint32_t applyMode;
    };
    /** Defines the structure of type Download_Request */
    typedef struct Download_Request DownloadRequest;

    /**
     * represents the Transfer Complete Message Structure.
     */
    struct Transfer_Complete_Message
    {
        /** Transfer Start Time. */
        std::string StartTime;
        /** Transfer Complete Time. */
        std::string CompleteTime;
        /** Command key. */
        std::string CommandKey;
        /** Transfer Fault Code. */
        std::string FaultCode;
        /** Transfer Fault string. */
        std::string FaultString;
        /** Transfer Status. */
        std::string Status;
    };

    typedef struct Transfer_Complete_Message TransferCompleteMessage;

    /**
     * represents the Package Attribute Info Structure.
     */
    struct Package_Attribute_Info
    {
        /** Package Version.*/
        std::string version;
        /** Package ID. */
        std::string id;
        /** Package Type. */
        std::string type;
        /** Package name. */
        std::string name;
    };

    typedef struct Package_Attribute_Info PackageAttributeInfo;

    /**
     * get the reference to the software upgrade manager instance.
     *
     * @return a reference to the SoftwareUpgradeManager singleton.
     */
    static SoftwareUpgradeManager& instance ();

    /**
     * This method retrieves the current State of the SW Upgrade state machine.
     * @param state current software upgrade state.
     * @return bool the response value.
     */
    bool getDownloadState (IARM_Bus_SWUPMgr_SWUpgrade_State_t& state);

    /**
     * Common Interface Invoked across all the interfaces { ACS | LAN | LOCALFS }.
     * @param downloadRequest represents DownloadRequest parameters.
     */
    void invokeSWUpgradeService (DownloadRequest downloadRequest);

    /**
     * This method is used to notify the listeners that change in software upgrade state.
     */
    void onSWUpgradeStateChange ();

    void setSoftwareUpgradeState (const SWUpgradeStateChangeInfo info);

    /**
     * This method intimates the Stack that it may continue to install a SW Upgrade image that has been downloaded.
     * @return bool denotes the installation process status.
     */
    virtual InstallSWImageResult installSWImage (bool afterReboot) = 0;

protected:
    SoftwareUpgradeManager ();
    /**
     * This method sets the current State of the SW Upgrade state machine.
     * @param state current software upgrade state.
     */
    //void setSoftwareUpgradeState (const SWUpgradeStateChangeInfo info);

private:
    /**
     * represents the current software upgrade state.
     */
    static StateMachine currentSWUpgradeState;
    /**
     * represents the previous software upgrade state.
     */
    static StateMachine previousSWUpgradeState;

    /**
     * represents the SWUpgrade Commands.
     */
    std::queue <SWUpgradeCommand*> swUpCmdQ;

    /**
     * represents the SWUpgradeStateChangeEvent Queue
     */
    std::queue <SWUpgradeStateChangeEvent*> swUpStateChangeEventQ;

    /**
     * represents the SWUpgrade CommandMap.
     */
    static SWUpgradeCommandMap commandMap;

    /**
     *  pthread mutex lock and condition wait on SW Upgrade Command Queue.
     */
    pthread_mutex_t swUpCmdQLock;
    pthread_cond_t swUpCmdQCV;

    /**
     *  pthread mutex lock and condition wait on expiry timer.
     */
    pthread_mutex_t timerLock;
    static pthread_cond_t isTimerExpired;

    /**
     *  pthread mutex lock and condition wait on SW Upgrade StateChangeEvent Queue.
     */
    pthread_mutex_t swUpStateChangeEventQLock;
    pthread_cond_t swUpStateChangeEventQCV;

    static bool eaRestartRequired;
    static int downloadProgressStatus;

    SWUpgradeCommand* command;
    BundleAttributeInfo bundleAttrInfo;
    PackageAttributeInfo* packageAttrInfo;

    SWUpgradeStateChangeEvent* event;

    uint32_t faultCode;
    std::string faultString;

    /**
     *  This function used to process the SWUpgradeCommands.
     *  @return object denotes the SWUpgradeCommand.
     */
    static void* run (void* object);

    /**
     *  This function receives the SWUpgradeCommand from the queue.
     *  @return object denotes the SWUpgradeCommand.
     */
    SWUpgradeCommand* receiveSWUpgradeCommand ();

    /**
     *  This function sends the SWUpgradeCommand to queue.
     *  @param command denotes the SWUpgradeCommand.
     */
    void sendSWUpgradeCommand (SWUpgradeCommand* command);

    /**
     *  This function receives the SWUpgradeStateChangeEvent from the queue.
     *  @return object denotes the SWUpgradeStateChangeEvent.
     */
    SWUpgradeStateChangeEvent* receiveSWUpStateChangeEvent ();

    /**
     *  This function sends the SWUpgradeStateChangeEvent to queue.
     *  @param event denotes the SWUpgradeStateChangeEvent.
     */
    void sendSWUpStateChangeEvent (SWUpgradeStateChangeEvent* event);

    /**
     *  This function sets the SoftwareVersion with the bundle version on successful download.
     *  @return bool denotes response value.
     */
    bool updateSoftwareVersion ();

    /**
     *  This function sends the TransferComplete Message to ACS.
     *  @param command denotes SWUpgradeCommand.
     *  @return bool denotes response value.
     */
    bool sendTransferCompleteMessageToACS (SWUpgradeCommand* command);

    /**
     * This function deletes the SWUpgradeCommand class pointer.
     */
    bool deleteSWUpgradeCommand ();

    /**
     *  This function sets the error value/reason.
     *  @param faultCode denotes error value as integer.
     *  @param faultString denotes failure reason as string.
     */
    void setErrorValues (const uint32_t& faultCode, const std::string& faultString);
    /**
     *  This function gets the error value/reason.
     *  @param faultCode denotes error value as integer.
     *  @param faultString denotes failure reason as string.
     */
    void getErrorValues (uint32_t& faultCode, std::string& faultString);

    /**
     * This method initializes the HAL implementation of
     * PlatformSoftwareManager.
     */
    virtual void initHAL () = 0;

    /**
     * This method deinitializes the HAL implementation of
     * PlatformSoftwareManager.
     */
    virtual void deinitHAL () = 0;

    /**
     * This method is used to Downloads the SW Image from the Server.
     * @param url denotes complete url path to the SW Image.
     * @param targetFileName denotes file path to save SW Image.
     * @return int denotes the download response value.
     */
    virtual DownloadSWImageResult downloadSWImage (const std::string& url, const std::string& userName, const std::string& password, const uint32_t fileSize, const std::string& targetFileName) = 0;

    /**
     * This method cancels the current SWUpgrade process.
     * The SW Image should be reverted to the previous SW Image
     * after calling this method and the STB may reboot if necessary.
     */
    virtual void cancelSWUpgrade () = 0;
};
#endif /* SOFTWAREUPGRADEMANAGER_H_ */
