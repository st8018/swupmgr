/*
 * SWUpgradeCommand.h
 *
 */

#ifndef SWUPGRADECOMMAND_H_
#define SWUPGRADECOMMAND_H_

#include <time.h>
#include <stdint.h>
#include <cstdio>
#include <string>

class SWUpgradeCommand
{
    friend class SoftwareUpgradeManager;
    friend class PlatformSoftwareUpgradeManager;

public:

    SWUpgradeCommand ();
    virtual ~SWUpgradeCommand ();

private:
    // command Key
    std::string commandKey;

    // type of download
    uint32_t downloadType;

    // source url
    std::string swUpgradeImageURL;

    struct SWUpgrade_Time_Window
    {
        // offset in seconds
        time_t applyStartTime;
        time_t applyStopTime;

        // apply mode
        uint32_t scheduleDownloadMode;
    };

    SWUpgrade_Time_Window swUpgrade_Time_Window;

    // faultCode
    uint32_t faultCode;

    // faultString
    std::string faultString;
};

#endif /* SWUPGRADECOMMAND_H_ */
