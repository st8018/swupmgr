# The header.mk should be included at the top line.
include $(RDK_ROOT)/tpc/build/common/header.mk

TARGET_MGR_NAME = swupMgr

TARGET_BIN = $(BIN_DIR)/$(TARGET_MGR_NAME)Main
BUILD_DIRS = $(BIN_DIR) $(OBJ_DIR)
TEST_MAIN = $(BIN_DIR)/$(TARGET_MGR_NAME)Test

INC_DIRS = ./include
INC_DIRS += ./iarm/include
INC_DIRS += ./hal/include
INC_DIRS += ./iarm/src
INC_DIRS += $(WATCHDOG_PATH)/include
CFLAGS +=
CCFLAGS +=
LDFLAGS += -lpthread

USE_IARM_BUS := TRUE
USE_RDK_LOGGER := TRUE

SWUP_SCRIPTS = ./hal/scripts/*.sh

IARM_SRC_DIRS = ./iarm/src
IARM_SRC_FILES = $(foreach d,$(IARM_SRC_DIRS),$(wildcard $(addprefix $(d)/,$(SRC_EXTS))))
IARM_OBJ_FILES = $(addprefix $(OBJ_DIR)/,$(addsuffix $(OBJ_EXT),$(basename $(notdir $(IARM_SRC_FILES)))))

HAL_SRC_DIRS = ./hal/src
HAL_SRC_FILES = $(foreach d,$(HAL_SRC_DIRS),$(wildcard $(addprefix $(d)/,$(SRC_EXTS))))
HAL_OBJ_FILES = $(addprefix $(OBJ_DIR)/,$(addsuffix $(OBJ_EXT),$(basename $(notdir $(HAL_SRC_FILES)))))

TEST_SRC_DIRS = ./test
TEST_SRC_FILES = $(foreach d,$(TEST_SRC_DIRS),$(wildcard $(addprefix $(d)/,$(SRC_EXTS))))
TEST_OBJ_FILES = $(addprefix $(OBJ_DIR)/,$(addsuffix $(OBJ_EXT),$(basename $(notdir $(TEST_SRC_FILES)))))

vpath %.c $(IARM_SRC_DIRS)
vpath %.cpp $(IARM_SRC_DIRS)

vpath %.c $(HAL_SRC_DIRS)
vpath %.cpp $(HAL_SRC_DIRS)

vpath %.c $(TEST_SRC_DIRS)
vpath %.cpp $(TEST_SRC_DIRS)

all: $(BUILD_DIRS) $(TARGET_BIN) $(TEST_MAIN) install

$(TARGET_BIN): $(HAL_OBJ_FILES) $(OBJ_FILES) $(IARM_OBJ_FILES)
	$(CXX) -o $@ $(IARM_OBJ_FILES) $(OBJ_FILES) $(HAL_OBJ_FILES) $(LDFLAGS)

clean: clean_build_dirs

$(TEST_MAIN): $(TEST_OBJ_FILES)
	$(CXX) -o $@ $(TEST_OBJ_FILES) $(LDFLAGS)

install: install_target_bin
ifneq (x$(OUTPUT_BIN_DIR), x)
	cp -uf $(TEST_MAIN) $(OUTPUT_BIN_DIR)
	cp -uf $(SWUP_SCRIPTS) $(OUTPUT_BIN_DIR)
endif

# The footer.mk should be included at the bottom line.
include $(RDK_ROOT)/tpc/build/common/footer.mk
