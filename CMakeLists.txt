cmake_minimum_required(VERSION 2.8)
project (SoftwareUpgradeMgr)

find_package(Threads)

add_compile_options(-W -Wall -g -O2 -fPIC)
add_compile_options(-std=c++0x)

include_directories(
    $ENV{TPC_INCDIR}/swupmgr
    $ENV{TPC_INCDIR}/watchdog
    $ENV{SYSROOT_INCDIR}/
    $ENV{RDK_INCDIR}/iarmbus
    $ENV{RDK_INCDIR}/logger
)

file(GLOB swupMgr_SRC
    "src/*.h"
    "src/*.cpp"
)


file(GLOB test_SRC
    "test/*.c"
    "test/*.cpp"
)

file(GLOB iarm_SRC
    "iarm/include/*.h"
    "iarm/src/*.c"
    "iarm/src/*.cpp"
)

link_directories( 
  $ENV{TARGET_LIBDIR} 
)

add_executable( swupMgrMain ${swupMgr_SRC} ${iarm_SRC} )

## Create a virtual container for object files needed by the test executable.
## Note: this does not generate a static library
add_library(swupMgrObjects OBJECT 
    "src/SoftwareUpgradeManager.cpp" 
    "src/SWUpgradeCommand.cpp"
    "src/SWUpgradeStateChangeEvent.cpp" 
    "iarm/src/swupMgr.cpp"
)

add_executable( swupMgrTest ${test_SRC} $<TARGET_OBJECTS:swupMgrObjects> ) 

target_link_libraries( swupMgrMain swupmgrhal rdkloggers IARMBus ${CMAKE_THREAD_LIBS_INIT} )
target_link_libraries( swupMgrTest swupmgrhal rdkloggers IARMBus ${CMAKE_THREAD_LIBS_INIT} )

install(TARGETS swupMgrMain DESTINATION bin)
## Should only install conditionally - we don't want the test binaries in the production rootfs
#install(TARGETS swuMgrTest DESTINATION bin)

