/*
 * SWUpgradeStateChangeEvent.cpp
 *
 */

#include "SWUpgradeStateChangeEvent.h"
#include "SoftwareUpgradeManager.h"

SWUpgradeStateChangeEvent::SWUpgradeStateChangeEvent ():
    state(SWUPGRADE_STATE_IDLE), faultCode(SoftwareUpgradeManager::FAULTCODE_NO_FAULT),
    faultString("")
{
}

SWUpgradeStateChangeEvent::~SWUpgradeStateChangeEvent ()
{
}
