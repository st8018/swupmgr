/*
 * SoftwareUpgradeManager.cpp
 *
 */

#include <sstream>
#include <stdlib.h>
#include <unistd.h>

#include "swupMgrInternal.h"
#include "SoftwareUpgradeManager.h"
#include "PlatformSoftwareUpgradeManager.h"

#define REBOOT_WAIT_TIME 120

static IARM_Bus_SWUPMgr_SWUpgrade_State_t convertStateMachineToIARMSWUpgradeState(StateMachine state);

StateMachine SoftwareUpgradeManager::currentSWUpgradeState;
StateMachine SoftwareUpgradeManager::previousSWUpgradeState;

SWUpgradeCommandMap SoftwareUpgradeManager::commandMap;

bool SoftwareUpgradeManager::eaRestartRequired;
int SoftwareUpgradeManager::downloadProgressStatus;

SoftwareUpgradeManager& SoftwareUpgradeManager::instance ()
{
    static PlatformSoftwareUpgradeManager platformSoftwareUpgradeManager;
    return platformSoftwareUpgradeManager;
}

void SoftwareUpgradeManager::init ()
{
    SoftwareUpgradeManager::instance().initHAL();
}

void SoftwareUpgradeManager::deinit ()
{
    SoftwareUpgradeManager::instance().deinitHAL();
}

SoftwareUpgradeManager::SoftwareUpgradeManager ()
{
    // Set the Current/Previous SWUpgrade State to IDLE
    currentSWUpgradeState = SWUPGRADE_STATE_IDLE;
    previousSWUpgradeState = SWUPGRADE_STATE_IDLE;

    eaRestartRequired = false;

    pthread_mutex_init (&swUpCmdQLock, NULL);
    pthread_cond_init (&swUpCmdQCV, NULL);

    pthread_mutex_init (&swUpStateChangeEventQLock, NULL);
    pthread_cond_init (&swUpStateChangeEventQCV, NULL);

    // run the SoftwareUpgradeManager in its own thread
    pthread_t thread;
    if (pthread_create (&thread, NULL, &SoftwareUpgradeManager::run, this) != 0)
    {
        throw std::runtime_error ("Error starting software upgrade manager thread");
    }
}

SoftwareUpgradeManager::~SoftwareUpgradeManager ()
{
    pthread_mutex_destroy (&swUpStateChangeEventQLock);
    pthread_cond_destroy (&swUpStateChangeEventQCV);

    pthread_mutex_destroy (&swUpCmdQLock);
    pthread_cond_destroy (&swUpCmdQCV);
}

void SoftwareUpgradeManager::sendSWUpgradeCommand (SWUpgradeCommand* command)
{
    pthread_mutex_lock (&swUpCmdQLock);
    swUpCmdQ.push (command);
    pthread_cond_signal (&swUpCmdQCV);
    pthread_mutex_unlock (&swUpCmdQLock);
}

SWUpgradeCommand* SoftwareUpgradeManager::receiveSWUpgradeCommand ()
{
    pthread_mutex_lock (&swUpCmdQLock);

    while (swUpCmdQ.empty ())
    {
        pthread_cond_wait (&swUpCmdQCV, &swUpCmdQLock);
    }

    SWUpgradeCommand* command = swUpCmdQ.front ();

    swUpCmdQ.pop ();

    pthread_mutex_unlock (&swUpCmdQLock);

    return command;
}

void SoftwareUpgradeManager::sendSWUpStateChangeEvent (SWUpgradeStateChangeEvent *event)
{
    pthread_mutex_lock (&swUpStateChangeEventQLock);
    swUpStateChangeEventQ.push (event);
    pthread_cond_signal (&swUpStateChangeEventQCV);
    pthread_mutex_unlock (&swUpStateChangeEventQLock);
}

SWUpgradeStateChangeEvent* SoftwareUpgradeManager::receiveSWUpStateChangeEvent ()
{
    pthread_mutex_lock (&swUpStateChangeEventQLock);

    while (swUpStateChangeEventQ.empty ())
    {
        pthread_cond_wait (&swUpStateChangeEventQCV, &swUpStateChangeEventQLock);
    }

    SWUpgradeStateChangeEvent* event = swUpStateChangeEventQ.front ();

    swUpStateChangeEventQ.pop ();

    pthread_mutex_unlock (&swUpStateChangeEventQLock);

    return event;
}

void SoftwareUpgradeManager::setSoftwareUpgradeState (const SWUpgradeStateChangeInfo info)
{
    SWUpgradeStateChangeEvent *event = new SWUpgradeStateChangeEvent();

    printf("setSoftwareUpgradeState state=%d\n", info.state);

    event->state = info.state;
    event->faultCode = info.faultCode;
    event->faultString = info.faultString;

    printf("setSoftwareUpgradeState,sending SWUpgradeStateChangeEvent\n");
    sendSWUpStateChangeEvent(event);
}

bool SoftwareUpgradeManager::getDownloadState (IARM_Bus_SWUPMgr_SWUpgrade_State_t& state)
{
    printf("getDownloadState ::  currentSWUpgradeState :: %d\n", currentSWUpgradeState);
    state = convertStateMachineToIARMSWUpgradeState(currentSWUpgradeState);
    return true;
}

bool SoftwareUpgradeManager::sendTransferCompleteMessageToACS (SWUpgradeCommand* swUpgradeCommand)
{
    TransferCompleteMessage transferCompleteMsg;
    std::stringstream faultCode;

    switch (swUpgradeCommand->downloadType)
    {
        case SoftwareUpgradeManager::DOWNLOAD:
        {
        }
            break;
        case SoftwareUpgradeManager::SCHEDULE_DOWNLOAD:
        {
        }
            break;
        default:
            break;
    }

    faultCode << swUpgradeCommand->faultCode;

    transferCompleteMsg.FaultCode = faultCode.str ();
    transferCompleteMsg.FaultString = swUpgradeCommand->faultString;
    transferCompleteMsg.CommandKey = swUpgradeCommand->commandKey;
    transferCompleteMsg.Status = transferCompleteMessage;

    //sendTransferCompleteMessage (transferCompleteMsg);

    if (swUpgradeCommand->downloadType == SoftwareUpgradeManager::SCHEDULE_DOWNLOAD)
    {
        // remove the command from commandMap
        commandMap.erase (swUpgradeCommand->swUpgrade_Time_Window.applyStartTime);
    }

    return true;
}

bool SoftwareUpgradeManager::deleteSWUpgradeCommand ()
{
    if (command != NULL)
        delete command;

    if (packageAttrInfo != NULL)
        delete packageAttrInfo;

    if (!bundleAttrInfo.empty ())
        bundleAttrInfo.clear ();

    return true;
}

bool SoftwareUpgradeManager::updateSoftwareVersion ()
{
    std::string bundleVersion;
    for (std::map <std::string, std::string>::const_iterator it = bundleAttrInfo.begin (); it != bundleAttrInfo.end ();
        ++it)
    {
        if (it->first.compare ("bundleVersion") == 0)
            bundleVersion = it->second.c_str ();
    }

    printf("updateSoftwareVersion :: bundleVersion :: %s\n", bundleVersion.c_str ());
    return false;
    //return DataInterfaceManager::instance ().setTR069ParameterValue ("Device.DeviceInfo.SoftwareVersion", bundleVersion);
}

void SoftwareUpgradeManager::setErrorValues (const uint32_t& errorCode, const std::string& errorMsg)
{
    faultCode = errorCode;
    faultString = errorMsg;
}

void SoftwareUpgradeManager::getErrorValues (uint32_t& errorCode, std::string& errorMsg)
{
    errorCode = faultCode;
    errorMsg = faultString;
}

void SoftwareUpgradeManager::invokeSWUpgradeService (DownloadRequest downloadRequest)
{
    SWUpgradeCommand* swUpgradeCommand = new SWUpgradeCommand ();

    swUpgradeCommand->commandKey = downloadRequest.commandKey;
    swUpgradeCommand->downloadType = downloadRequest.downloadType;
    swUpgradeCommand->swUpgradeImageURL = downloadRequest.imageUrl;
    printf("invokeSWUpgradeService :: swUpgradeImageURL :: %s\n", swUpgradeCommand->swUpgradeImageURL.c_str ());

    switch (downloadRequest.downloadType)
    {
        case SoftwareUpgradeManager::DOWNLOAD:
        {
            swUpgradeCommand->swUpgrade_Time_Window.applyStartTime = time (0);
            swUpgradeCommand->swUpgrade_Time_Window.applyStopTime = time (0);
            sendSWUpgradeCommand (swUpgradeCommand);
        }
            break;
        case SoftwareUpgradeManager::SCHEDULE_DOWNLOAD:
        {
            swUpgradeCommand->swUpgrade_Time_Window.scheduleDownloadMode = downloadRequest.applyMode;

            time_t currentTime;
            time (&currentTime);

            swUpgradeCommand->swUpgrade_Time_Window.applyStartTime = currentTime + downloadRequest.applyStartTime;
            swUpgradeCommand->swUpgrade_Time_Window.applyStopTime = currentTime + downloadRequest.applyStopTime;

            printf("invokeSWUpgradeService ::  applyStartTime :: %ld\n",
                swUpgradeCommand->swUpgrade_Time_Window.applyStartTime);
            printf("invokeSWUpgradeService ::  applyStopTime  :: %ld\n",
                swUpgradeCommand->swUpgrade_Time_Window.applyStopTime);

            printf("invokeSWUpgradeService ::  acquireStartTime :: %d\n", downloadRequest.acquireStartTime);
            printf("invokeSWUpgradeService ::  acquireStopTime  :: %d\n", downloadRequest.acquireStopTime);

            commandMap.insert (
                std::pair <time_t, SWUpgradeCommand*> (swUpgradeCommand->swUpgrade_Time_Window.applyStartTime,
                    swUpgradeCommand));

        }
            break;
        default:
            break;
    }
}

static IARM_Bus_SWUPMgr_SWUpgrade_State_t convertStateMachineToIARMSWUpgradeState(StateMachine state)
{
    IARM_Bus_SWUPMgr_SWUpgrade_State_t iarmState;

    switch (state)
    {
        case SWUPGRADE_STATE_IDLE:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_IDLE;
            break;
        case SWUPGRADE_STATE_ACQUIRING_IMAGE:
        case SWUPGRADE_STATE_WAITING_FOR_DOWNLOAD:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_DOWNLOADING;
            break;
        case SWUPGRADE_STATE_UNPACKING_IMAGE:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_DOWNLOADED;
            break;
        case SWUPGRADE_STATE_LOADING_IMAGE:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_CONFIGURING;
            break;
        case SWUPGRADE_STATE_WAITING_TO_FLASH:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_PREPARING;
            break;
        case SWUPGRADE_STATE_FLASHING_IMAGE:
        case SWUPGRADE_STATE_WAITING_FOR_COMPLETING:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_WRITING;
            break;
        case SWUPGRADE_STATE_COMPLETED:
        case SWUPGRADE_STATE_WAITING_REBOOT_EVENT:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_COMPLETED;
            break;
        case SWUPGRADE_STATE_WAITING_FOR_REBOOT:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_REBOOTING;
            break;
        case SWUPGRADE_STATE_FAILED:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_FAILED;
            break;
        default:
            iarmState = IARM_BUS_SWUPMGR_SWUPGRADE_STATE_IDLE;
            break;
    }

    return iarmState;
}

void SoftwareUpgradeManager::onSWUpgradeStateChange ()
{
    printf("onSWUpgradeStateChange Invoked\n");
    broadcastSWUpgradeStateChange(
            convertStateMachineToIARMSWUpgradeState(currentSWUpgradeState), 
            convertStateMachineToIARMSWUpgradeState(previousSWUpgradeState));
}

void* SoftwareUpgradeManager::run (void* object)
{
    SoftwareUpgradeManager* swUpgradeManager = (SoftwareUpgradeManager*) object;

    bool status = false;
    uint32_t errorCode;
    std::string errorString;

    while (1) {
        try {
            printf("run :: currentSWUpgradeState  :: %d\n", currentSWUpgradeState);
            printf("run :: previousSWUpgradeState :: %d\n", previousSWUpgradeState);

            if (currentSWUpgradeState != SWUPGRADE_STATE_IDLE &&
                    currentSWUpgradeState != SWUPGRADE_STATE_ACQUIRING_IMAGE &&
                    currentSWUpgradeState != SWUPGRADE_STATE_FLASHING_IMAGE) {
                swUpgradeManager->event = NULL;
                // Wait here until a new SW Upgrade State Change Event is received from HAL
                printf("run :: wating here until SWUpgradeStateChangeEvent is received\n");
                swUpgradeManager->event = swUpgradeManager->receiveSWUpStateChangeEvent ();
                currentSWUpgradeState = swUpgradeManager->event->state;
                printf("run :: SWUpgradeStateChangeEvent is received,currentSWUpgradeState=%d\n",
                        currentSWUpgradeState);
            }

            if (previousSWUpgradeState != currentSWUpgradeState) {
                swUpgradeManager->onSWUpgradeStateChange();
                previousSWUpgradeState = currentSWUpgradeState;
                // For Test to make sure that event is delivered before next step starts.
                usleep(2*1000*1000);
            }

            switch (currentSWUpgradeState) {
                case SWUPGRADE_STATE_IDLE:
                    {
                        swUpgradeManager->command = NULL;
                        // wait here until a new SW Upgrade Command is received
                        printf("run :: wating here until SWUpgradeCommand is received\n");
                        swUpgradeManager->command = swUpgradeManager->receiveSWUpgradeCommand ();
                        printf("run :: SWUpgradeCommand is received\n");

                        if (swUpgradeManager->command != NULL) {
                            printf("run :: Changing state to SWUPGRADE_STATE_ACQUIRING_IMAGE to download a SW Image\n");
                            currentSWUpgradeState = SWUPGRADE_STATE_ACQUIRING_IMAGE;
                        }
                    }
                    break;
                case SWUPGRADE_STATE_ACQUIRING_IMAGE:
                    {
                        // Trying to download the SW image
                        DownloadSWImageResult result = swUpgradeManager->downloadSWImage (
                                swUpgradeManager->command->swUpgradeImageURL, "", "", 0, "");
                        printf("run :: DownloadSWImageResult=%d\n", result);

                        if (result == DOWNLOAD_SW_IMAGE_NOT_STARTED) {
                            currentSWUpgradeState = SWUPGRADE_STATE_FAILED;
                            // TODO: How to set FaultCode and FaultString
                        } else {
                            currentSWUpgradeState = SWUPGRADE_STATE_WAITING_FOR_DOWNLOAD;
                        }
                    }
                    break;
                // This state is set from PlatformSoftwareUpgradeManager.
                case SWUPGRADE_STATE_UNPACKING_IMAGE:
                    {
                        // clear the bundleAttrInfo map contents which no longer required
                        if (!swUpgradeManager->bundleAttrInfo.empty ())
                        {
                            swUpgradeManager->bundleAttrInfo.clear ();
                        }
                    }
                    break;
                case SWUPGRADE_STATE_FLASHING_IMAGE:
                    usleep(2*1000*1000);
                    currentSWUpgradeState = SWUPGRADE_STATE_WAITING_FOR_COMPLETING;
                    break;
                case SWUPGRADE_STATE_COMPLETED:
                    {
                        // Send the ACS a Transfer Complete Message
                        swUpgradeManager->command->faultCode = 0;
                        swUpgradeManager->command->faultString = "";
                        status = swUpgradeManager->sendTransferCompleteMessageToACS (swUpgradeManager->command);

                        status = swUpgradeManager->updateSoftwareVersion ();

                        currentSWUpgradeState = SWUPGRADE_STATE_WAITING_REBOOT_EVENT;
                    }
                    break;
                case SWUPGRADE_STATE_WAITING_FOR_REBOOT:
                    {
                        // Send the ACS a Transfer Complete Message
                        swUpgradeManager->command->faultCode = 0;
                        swUpgradeManager->command->faultString = "";
                        status = swUpgradeManager->sendTransferCompleteMessageToACS (swUpgradeManager->command);

                        status = swUpgradeManager->deleteSWUpgradeCommand ();
                        currentSWUpgradeState = SWUPGRADE_STATE_IDLE;

                        while (1) {
                            //TODO what condition will break?
                            usleep(10*1000*1000);
                            printf("Waiting for reboot...\n");
                        }
                    }
                    break;
                case SWUPGRADE_STATE_FAILED:
                    {
                        // Send the ACS a Transfer Complete Message
                        swUpgradeManager->getErrorValues (errorCode, errorString);
                        swUpgradeManager->command->faultCode = errorCode;
                        swUpgradeManager->command->faultString = errorString;
                        status = swUpgradeManager->sendTransferCompleteMessageToACS (swUpgradeManager->command);

                        status = swUpgradeManager->deleteSWUpgradeCommand ();
                        currentSWUpgradeState = SWUPGRADE_STATE_IDLE;
                    }
                    break;
                default:
                    break;
            }
        } catch (std::exception& e) {
            printf("exception raised in state %d\n", currentSWUpgradeState);

            switch (currentSWUpgradeState) {
                case SWUPGRADE_STATE_ACQUIRING_IMAGE:
                    swUpgradeManager->setErrorValues (FAULTCODE_FT_FAILURE, "exception : File Transfer Failure");
                    break;
                case SWUPGRADE_STATE_LOADING_IMAGE:
                    swUpgradeManager->setErrorValues (FAULTCODE_INTERNAL_ERROR,
                        "Internal error: exception in Loading Image");
                    break;
                case SWUPGRADE_STATE_FLASHING_IMAGE:
                    swUpgradeManager->setErrorValues (FAULTCODE_INTERNAL_ERROR,
                        "Internal error: exception in Flashing Image");
                    break;
                default:
                    break;
            }
            currentSWUpgradeState = SWUPGRADE_STATE_FAILED;
        } // end catch
    } // end while

    return 0;
}
